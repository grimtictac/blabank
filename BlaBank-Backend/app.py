from blabank_flask import app
from blabank_flask import db
from blabank_flask import test_data 
import sys

if len(sys.argv) == 2 and sys.argv[1] == "--init":
    print("Initialising Database")
    db.create_all()
    test_data.create_test_data()

if __name__ == '__main__':
    app.run(host='0.0.0.0')