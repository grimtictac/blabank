from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Table, Column, String, MetaData, Integer, ForeignKey, DateTime
from sqlalchemy.orm import relationship, backref
from blabank_flask import db

class Account(db.Model):
    id      = db.Column(db.Integer, primary_key=True)
    type    = db.Column(db.String , nullable=False)
    balance = db.Column(db.Integer, default=0)

    def as_dict(self):
       return { "id"      : self.id,  
                "type"    : self.type, 
                "balance" : self.balance }