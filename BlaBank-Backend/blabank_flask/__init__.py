from flask import Flask, jsonify, abort, request, make_response, url_for
from os import environ
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
from sqlalchemy import event

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///blabank.db"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_ECHO'] = True

db = SQLAlchemy(app)

import blabank_flask.views




