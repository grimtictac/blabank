from blabank_flask.models import Account
import blabank_flask.transactions as t
from blabank_flask import db

def create_test_data():        
        acc1 = Account( id=1, type=t.ACCOUNT_TYPE_CURRENT, balance=100 )
        acc2 = Account( id=2, type=t.ACCOUNT_TYPE_CURRENT, balance=100 )
        acc3 = Account( id=3, type=t.ACCOUNT_TYPE_CURRENT, balance=100 )
        acc4 = Account( id=4, type=t.ACCOUNT_TYPE_SAVINGS, balance=100 )
        acc5 = Account( id=5, type=t.ACCOUNT_TYPE_SAVINGS, balance=100 )
        acc6 = Account( id=6, type=t.ACCOUNT_TYPE_SAVINGS, balance=100 )

        db.session.add(acc1)
        db.session.add(acc2)
        db.session.add(acc3)
        db.session.add(acc4)
        db.session.add(acc5)
        db.session.add(acc6)

        db.session.commit()
        