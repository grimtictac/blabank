from blabank_flask import db
from blabank_flask.models import Account

TRANSACTION_TYPE_DEPOSIT    = "deposit"
TRANSACTION_TYPE_WITHDRAWAL = "withdrawal"

ACCOUNT_TYPE_CURRENT = "current"
ACCOUNT_TYPE_SAVINGS = "savings"

def perform_transaction( account_id, transaction_type, amount ):
    acc = Account.query.filter_by(id = account_id).first()

    if   transaction_type == TRANSACTION_TYPE_DEPOSIT:
        acc.balance += int(amount)
    elif transaction_type == TRANSACTION_TYPE_WITHDRAWAL:
        acc.balance -= int(amount)
    else:
        raise Exception("Unknown transaction type: " + str(transaction_type))

    db.session.add(acc)
    db.session.commit()

    return acc.as_dict()

    
