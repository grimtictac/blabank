from datetime import datetime
from flask import render_template, jsonify, request
from blabank_flask import app
import blabank_flask.transactions as transactions
from blabank_flask import db
from blabank_flask.models import Account
import requests, secrets

@app.route('/', methods=['GET'])
def list_accounts():

    accounts = Account.query.all()            
    results = []

    # Yes, it's terrible that we're unpacking and repacking the whole array here.
    # But I didn't want to spend too much time figuring out how to make SQLAlchemy 
    # models serialize arrays to JSON properly, so I just went for the simple approach.
    # This will cause insane memory problems for large lists...
    for acc in accounts:
        results.append( acc.as_dict() )
 
    return jsonify(results), 200

@app.route('/<id>', methods=['GET'])
def view_account(id):
     acc = Account.query.filter_by(id = id).first()

     return jsonify( acc.as_dict() )

@app.route('/d/<id>/<amount>', methods=['POST'])
def deposit(id, amount):

    resulting_account_state = transactions.perform_transaction(id, transactions.TRANSACTION_TYPE_DEPOSIT, amount)

    return jsonify( resulting_account_state )

@app.route('/w/<id>/<amount>', methods=['POST'])
def withdraw(id, amount):

    resulting_account_state = transactions.perform_transaction(id, transactions.TRANSACTION_TYPE_WITHDRAWAL, amount)

    return jsonify( resulting_account_state )