# BlaBank App

## Getting Started
* Install Pipenv
```
pip install pipenv
```

* Install packages from Pipfile
```
pipenv install
```

* Activate Virtualenv, launch flask app and initialise database
```
pipenv shell
python app.py --init 
```

(use --init on first run only, in future just run app.py)

After this you should have a virtualenv with flask installed (on windows the virtualenv will be located at C:\Users\<username>\.virtualenvs). If you have any issues with the virtualenv you can double check the documentation here:
https://docs.pipenv.org/#install-pipenv-today
https://docs.pipenv.org/basics/#example-pipenv-workflow

If you have *many* issues with the virtualenv (and you don't mind polluting your site-packages folder) you can just run:
$ pip install flask

